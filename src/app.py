from flask import Flask, request, jsonify, Response
from flask_pymongo import PyMongo
from werkzeug.security import generate_password_hash, check_password_hash
from bson import json_util
from bson.objectid import ObjectId

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/pythonapi"
mongo = PyMongo(app)

app = Flask(__name__)

@app.route('/users', methods=['POST'])
def create_user():
    print(request.json)

    name = request.json['name']
    email = request.json['email']
    password = request.json['password']

    if name and email and password:
        hashed_password = generate_password_hash(password)
        id = mongo.db.users.insert({
            'name': name,
            'email': email,
            'password': hashed_password
        })

        return {
            'id': str(id),
            'name': name,
            'email': email,
            'password': hashed_password
        }
    else:
        return not_found()

    return {'message': 'recibido'}

@app.route('/users', methods=['GET'])
def get_users():
   users = mongo.db.users.find()
   response = json_util.dumps(users)

   return Response(response, mimetype='application/json')

@app.route('/users/<id>', methods=['GET'])
def get_user(id):
    user = mongo.db.users.find_one({'_id': ObjectId(id)})
    response = json_util.dumps(user);

    return Response(response, mimetype='application/json')

@app.route('/users/<id>', methods=['DELETE'])
def delete_user(id):
    user = mongo.db.users.delete_one({'_id': ObjectId(id)})
    response = jsonify({'message': 'El usuario '+ id + ' fue eliminado'})

    return response

@app.route('/users/<id>', methods=['PUT'])
def update_user(id):
    name = request.json['name']
    email = request.json['email']
    password = request.json['password']

    if name and email and password:
        hashed_password = generate_password_hash(password)

        user = mongo.db.users.update_one({'_id': ObjectId(id)},{
            '$set': {
                'name': name,
                'email': email,
                'password': hashed_password
            }
        })

        response = jsonify({'message': 'El usuario '+ id + ' fue actualizado'})

        return response

@app.errorhandler(404)
def not_found(error=None):
    response = jsonify({
        'message': 'Recurso no encontrado: ' +request.url,
        'status': 404
    })
    response.status_code = 404

    return response
 

if __name__ == "__main__":
    app.run(debug=True)